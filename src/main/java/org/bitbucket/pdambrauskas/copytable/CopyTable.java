package org.bitbucket.pdambrauskas.copytable;

import org.bitbucket.pdambrauskas.copytable.commands.Command;
import org.bitbucket.pdambrauskas.copytable.commands.CopyRows;
import org.bitbucket.pdambrauskas.copytable.commands.TruncateTable;
import org.jooq.exception.DataAccessException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CopyTable {
    public enum Status { SUCCESS, FAIL }

    private static final Logger LOGGER = Logger.getLogger(App.class.getName());

    private String tableName;
    private Settings settings;

    public CopyTable(String tableName) {
        this.tableName = tableName;
    }

    public Status execute() {
        settings = Settings.getInstance();

        Connection sourceConnection = null;
        Connection destinationConnection = null;

        try {
            sourceConnection = getSourceConnection();
            destinationConnection = getDestinationConnection();

            destinationConnection.setAutoCommit(false);

            Command truncateTable = new TruncateTable(destinationConnection, tableName);
            Command copyTable = new CopyRows(sourceConnection, destinationConnection, tableName);

            truncateTable.execute();
            copyTable.execute();

            destinationConnection.commit();
            destinationConnection.setAutoCommit(true);
            return Status.SUCCESS;
        }
        catch (SQLException | DataAccessException exception) {
            rollback(destinationConnection);
            LOGGER.log(Level.SEVERE, "Copy Failed", exception);
            return Status.FAIL;
        }

        finally {
            closeConnections(sourceConnection, destinationConnection);
        }
    }

    private Connection getSourceConnection() throws SQLException {
        return DriverManager.getConnection(
                settings.get("sourceDb.url"),
                settings.get("sourceDb.user"),
                settings.get("sourceDb.password")
        );
    }

    private Connection getDestinationConnection() throws SQLException {
        return DriverManager.getConnection(
                settings.get("destinationDb.url"),
                settings.get("destinationDb.user"),
                settings.get("destinationDb.password")
        );
    }

    private void rollback(Connection connection) {
        if (connection != null) {
            try {
                connection.rollback();
            }

            catch (SQLException exception) {
                LOGGER.log(Level.SEVERE, "Rollback failed", exception);
            }
        }
    }

    private static void closeConnections(Connection... connections) {
        for (Connection connection : connections) {
            if (connection != null) {
                try {
                    connection.close();
                }

                catch (SQLException exception) {
                    LOGGER.log(Level.SEVERE, "Failed closing db connection", exception);
                }
            }
        }
    }
}


package org.bitbucket.pdambrauskas.copytable.commands;

public interface Command {
    void execute();
}

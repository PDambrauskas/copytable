package org.bitbucket.pdambrauskas.copytable.commands;

import org.jooq.*;
import org.jooq.impl.DSL;

import java.sql.Connection;

public class CopyRows implements Command {
    private final static int BATCH_SIZE = 1000;
    private Connection sourceConnection;
    private Connection destinationConnection;
    private String tableName;

    public CopyRows(Connection sourceConnection, Connection destinationConnection, String tableName) {
        this.sourceConnection = sourceConnection;
        this.destinationConnection = destinationConnection;
        this.tableName = tableName;
    }

    @Override
    public void execute() {
        Cursor<Record> cursor = sourceData();

        while (cursor.hasNext()) {
            Result<Record> records = cursor.fetch(BATCH_SIZE);
            copyToDestination(records);
        }
    }

    private Cursor<Record> sourceData() {
        return DSL.using(sourceConnection).select().from(tableName).fetchLazy();
    }

    private void copyToDestination(Result<Record> batch) {
        Table<Record> destinationTable = DSL.table(tableName);
        InsertValuesStepN<Record> chain = DSL.using(destinationConnection).insertInto(destinationTable, batch.fields());

        for(Record record : batch) {
            chain.values(record.intoList());
        }

        chain.execute();
    }
}



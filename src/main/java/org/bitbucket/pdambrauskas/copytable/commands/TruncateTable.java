package org.bitbucket.pdambrauskas.copytable.commands;

import org.jooq.impl.DSL;

import java.sql.Connection;

public class TruncateTable implements Command {
    private Connection connection;
    private String tableName;

    public TruncateTable(Connection connection, String tableName) {
        this.connection = connection;
        this.tableName = tableName;
    }

    @Override
    public void execute() {
        DSL.using(connection).truncate(tableName).execute();
    }
}

package org.bitbucket.pdambrauskas.copytable;

import java.util.logging.Level;
import java.util.logging.Logger;

public class App {
    private static final Logger LOGGER = Logger.getLogger(App.class.getName());

    public static void main(String[] args) {
        if (args.length == 0) {
            LOGGER.log(Level.SEVERE, "Table name not provided");
            System.exit(1);
        }

        String tableName = args[0];
        CopyTable copyTable = new CopyTable(tableName);

        switch (copyTable.execute()) {
            case SUCCESS:
                System.exit(0);
            case FAIL:
                System.exit(1);
        }
    }
}



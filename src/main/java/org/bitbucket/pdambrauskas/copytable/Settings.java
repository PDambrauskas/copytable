package org.bitbucket.pdambrauskas.copytable;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Settings {
    private static final Logger LOGGER = Logger.getLogger(Settings.class.getName());
    private static final String PROPERTIES_FILE_PATH = "/copytable.properties";
    private static Settings instance = new Settings();

    private Properties properties = new Properties();

    public static Settings getInstance() {
        return instance;
    }

    private Settings() {
        try {
            InputStream inputStream = Settings.class.getResourceAsStream(PROPERTIES_FILE_PATH);
            properties.load(inputStream);
        }

        catch (IOException exception) {
            LOGGER.log(Level.SEVERE, "Failed to load settings file", exception);
        }
    }

    public String get(String key) {
        return properties.getProperty(key);
    }
}
